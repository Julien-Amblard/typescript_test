const gulp 				= require('gulp');
//const babel 			= require('gulp-babel');
const watch 			= require('gulp-watch');
const ts 				= require('gulp-typescript');


gulp.task('js', () => {
    return gulp.src('src/*.ts')
    .pipe(ts({
        noImplicitAny: true
    }))
    .pipe(gulp.dest('build/'));
});

gulp.task('watch', () => {
	gulp.watch('scr/*', ['js']);
});


gulp.task('default', ['js', 'watch']);
