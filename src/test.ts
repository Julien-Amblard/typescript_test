const saySomething = ( _str : string ) => {
	alert( _str )
}
const myComponent = (() => {
	const say = ( _word : string ) => {
		alert( _word )
	}
	const init = () => {

	}
	return {
		say
	}
})();

myComponent.say('hello world')